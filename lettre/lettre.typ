#import "modele.typ": *
#show: lettre.with(
  sender: [
    Jean Dupont \
    123 route du chemin, \
    99234 Uneville \
    \
    Professionnel : 01.12.34.56.78 \
    Personnel :  06.12.34.56.78
  ],
  recipient: [
    Olivier Duchemin,\
    123 route du pont,\
    99567 Lautreville\
  ],
  date: [Uneville, le 3 septembre 2023,],
  subject: [Notre prochaine entrevue],
  name: [Jean Dupont],
)

// remplacrer les #lorem(123) par le texte de la lettre
#lorem(123)

#lorem(123)

Je vous prie d’agréer mes plus respectueuses salutations.
