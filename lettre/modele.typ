// This function gets your whole document as its `body`
// and formats it as a simple letter.
#let lettre(
  // Le nom et l'adresse de celui qui envoie
  sender: none,

  // L'adresse du destinataire
  recipient: none,

  // Le lieu et la date
  date: none,

  // Le sujet de la lettre
  subject: none,

  // Le nom à la fin de la lettre en dessous duquel est appossée la signature
  name: none,

  // Le contenu de la lettre
  body
) = {
  // Configuration globale de la page et du texte
  set page(margin: (top: 3cm))
  set text(lang: "fr", size: 12pt, font: "New Computer Modern")
  set par(leading: 0.55em, first-line-indent: 1.8em, justify: true)
  let vspace = 1cm

  // Alignement des adresses en haut
  grid(
  columns: (45%, 10%, 45%),
  sender,
  none,
  align(right, recipient)
  )

  v(vspace)

  // Display date. If there's no date add some hidden
  // text to keep the same spacing.
  align(right, if date != none {
      grid(
        columns: (60%, 40%),
        none,
        align(left, date)
      )
  } else {
    hide("a")
  })

  v(vspace)

  // Add the subject line, if any.
  if subject != none {
    par(leading: 0em, first-line-indent: 0em, justify: false)[
    *Objet* : #subject
    ]
    v(vspace)
  }

  // Add body and name.
  body
  v(vspace)
  grid(
    columns: (60%, 40%),
    none,
    align(left, name)
  )
}
